<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud() {
        return $this->render("gestion");
    }
    
    
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("count(*) total")
                ->distinct(),
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Numero de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
    }
        
    public function actionConsulta1() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 1 con DAO",
            "enunciado"=>"Numero de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("count(*) total")
                ->distinct()
                ->where("nomequipo='Banesto'")
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Numero de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }

    public function actionConsulta2() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista where nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero FROM ciclista where nomequipo="Banesto"',
            'totalCount'=>$numero,
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 2 con DAO",
            "enunciado"=>"Numero de ciclistas que hay en el equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("count(*) total")
                ->distinct()
                ->where("nomequipo='Banesto'")
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Numero de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }

    public function actionConsulta3() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) numero FROM ciclista',
            'totalCount'=>$numero,
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 3 con DAO",
            "enunciado"=>"La edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista",
        ]);
    }
    
    public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("avg(edad) total")
                ->distinct()
                ->where("nomequipo='Banesto'")
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los de equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }

    public function actionConsulta4() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) numero FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 4 con DAO",
            "enunciado"=>"La edad media de los de equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta5a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nomequipo,avg(edad) total")
                ->distinct()
                ->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta5() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct nomequipo) from ciclista')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nomequipo,AVG(edad) numero FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero'],
            "titulo"=> "Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("count(*) total,nomequipo")
                ->distinct()
                ->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>5,
            ]       
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total','nomequipo'],
            "titulo"=> "Consulta 6 con Active Record",
            "enunciado"=>"Numero de ciclistas por equipo",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
    }
    
    public function actionConsulta6() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct nomequipo) from ciclista')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero,nomequipo FROM ciclista group by nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero','nomequipo'],
            "titulo"=> "Consulta 6 con DAO",
            "enunciado"=>"Numero de ciclistas por equipo",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
    }
    
    public function actionConsulta7a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("count(*) total")
                ->distinct() 
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 7 con Active Record",
            "enunciado"=>"El numero total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
        ]);
    }
    
    public function actionConsulta7() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero FROM puerto',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
        ]);
    }
    
    public function actionConsulta8a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("count(*) total")
                ->distinct()
                ->where("altura>1500")
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 8 con Active Record",
            "enunciado"=>"El numero total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta8() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta9a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nomequipo")
                ->distinct()
                ->groupBy("nomequipo")
                ->having("count(*)>4"),
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    public function actionConsulta9() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(nomequipo) from ciclista GROUP BY nomequipo HAVING COUNT(*)>4')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    public function actionConsulta10a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nomequipo")
                ->distinct()
                ->where("edad between 28 and 32")
                ->groupBy("nomequipo")
                ->having("count(*)>4"),
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    public function actionConsulta10() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(nomequipo) from ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    public function actionConsulta11a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("dorsal,count(*) total")
                ->distinct()
                ->groupBy("dorsal"),
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=> "Consulta 11 con Active Record",
            "enunciado"=>"Indicame el numero de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal,COUNT(*) FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta11() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT COUNT(dorsal) FROM etapa')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT dorsal,COUNT(*) numero FROM etapa GROUP BY dorsal',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','numero'],
            "titulo"=> "Consulta 11 con DAO",
            "enunciado"=>"Indicame el numero de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal,COUNT(*) FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta12a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("dorsal")
                ->distinct()
                ->groupBy("dorsal")
                ->having("COUNT(*)>1"),
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 12 con Active Record",
            "enunciado"=>"Indicame el dorsal de los ciclistas que hayan ganado más de 1 etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1",
        ]);
    }
    
    public function actionConsulta12() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT COUNT(dorsal) FROM etapa GROUP BY dorsal HAVING COUNT(*)>1')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 12 con DAO",
            "enunciado"=>"Indicame el dorsal de los ciclistas que hayan ganado más de 1 etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1",
        ]);
    }
}